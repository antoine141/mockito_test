package by.shag.litvinchuk.maping;

import by.shag.litvinchuk.api.dto.UserDto;
import by.shag.litvinchuk.jpa.model.User;

public class UserDtoMapper {

    public User map(UserDto userDto) {
        throw new UnsupportedOperationException("map");
    }

    public UserDto map(User user) {
        throw new UnsupportedOperationException();
    }
}
