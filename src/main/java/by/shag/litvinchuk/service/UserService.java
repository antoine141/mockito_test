package by.shag.litvinchuk.service;

import by.shag.litvinchuk.api.dto.UserDto;
import by.shag.litvinchuk.jpa.model.User;
import by.shag.litvinchuk.jpa.reppsitory.UserRepository;
import by.shag.litvinchuk.maping.UserDtoMapper;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserRepository userRepository;

    private UserDtoMapper userDtoMapper;

    public UserDto saveUser(UserDto dto) {
        User user = userDtoMapper.map(dto);
        User save = userRepository.saveUser(user);
        return userDtoMapper.map(save);
    }

    public UserDto findById(Integer id) {
        User user = userRepository.findById(id);
        return userDtoMapper.map(user);
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream().map(user -> userDtoMapper.map(user))
                .collect(Collectors.toList());

    }

    public UserDto updateUser(UserDto dto) {
        User user = userDtoMapper.map(dto);
        User update = userRepository.updateUser(user);
        return userDtoMapper.map(update);
    }

    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }
}
