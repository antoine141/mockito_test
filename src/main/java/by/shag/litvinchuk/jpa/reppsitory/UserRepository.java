package by.shag.litvinchuk.jpa.reppsitory;

import by.shag.litvinchuk.jpa.model.User;

import java.util.List;

public class UserRepository {

    public User saveUser(User user) {
        throw new UnsupportedOperationException("saveUser");
    }

    public User findById(Integer id) {
        throw new UnsupportedOperationException("findById");
    }

    public List<User> findAll() {
        throw new UnsupportedOperationException("find All");
    }

    public User updateUser(User user) {
        throw new UnsupportedOperationException("update User");
    }

    public void deleteById(Integer id) {
        throw new UnsupportedOperationException("delete by Id");
    }
}
