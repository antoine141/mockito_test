package by.shag.litvinchuk.service;

import by.shag.litvinchuk.api.dto.UserDto;
import by.shag.litvinchuk.jpa.model.User;
import by.shag.litvinchuk.jpa.reppsitory.UserRepository;
import by.shag.litvinchuk.maping.UserDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDtoMapper userDtoMapper;

    @InjectMocks
    private UserService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(userDtoMapper, userRepository);
    }

    @Test
    void save() {
        User user = new User();
        user.setId(1);
        when(userDtoMapper.map(any(UserDto.class))).thenReturn(user);

        User saved = new User();
        saved.setName("Sergey");
        when(userRepository.saveUser(any(User.class))).thenReturn(saved);

        UserDto userDto = new UserDto();
        userDto.setLogin("nagibator");
        when(userDtoMapper.map(any(User.class))).thenReturn(userDto);


        UserDto result = service.saveUser(userDto);

        assertEquals(userDto, result);
        verify(userDtoMapper).map(userDto);
        verify(userRepository).saveUser(user);
        verify(userDtoMapper).map(saved);
    }

    @Test
    void findById() {
        User user = new User();
        user.setId(1);
        when(userRepository.findById(any(Integer.class))).thenReturn(user);

        UserDto dto = new UserDto();
        dto.setName("Alex");
        when(userDtoMapper.map(any(User.class))).thenReturn(dto);

        UserDto result = service.findById(1);

        assertEquals(dto, result);
        verify(userRepository).findById(1);
        verify(userDtoMapper).map(user);

    }

    @Test
    public void update() {
        User user = new User();
        user.setId(1);
        when(userDtoMapper.map(any(UserDto.class))).thenReturn(user);
        User update = new User();
        update.setName("Evgen");
        when(userRepository.updateUser(any(User.class))).thenReturn(update);
        UserDto dto = new UserDto();
        dto.setAge(28);
        when(userDtoMapper.map(any(User.class))).thenReturn(dto);

        UserDto result = service.updateUser(dto);

        assertEquals(dto, result);
        verify(userDtoMapper).map(dto);
        verify(userRepository).updateUser(user);
        verify(userDtoMapper).map(update);
    }

    @Test
    public void delete() {
        User user = new User();
        user.setId(1);
    }
}